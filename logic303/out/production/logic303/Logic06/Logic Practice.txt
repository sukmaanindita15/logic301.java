1. Buatlah deret angka yang terbentuk dari penjumlahan deret bilangan kelipatan 3 dikurang 1 dan deret bilangan kelipatan (-2) x 1. Angka pada index ganjil dari kedua deret bilangan tersebut saling dijumlahkan. Dan angka pada index genap dari kedua deret bilangan tersebut juga saling dijumlahkan. Index dimulai dari angka 0.
Input : Panjang array/panjang deret
Contoh : Dibawah ini hanya sekedar contoh yang menggunakan deret genap dan ganjil

Input panjang deret : 5
Deret genap : 0 2 4 6 8
Deret ganjil : 1 3 5 7 9
0 + 1 ; 2 + 3 ; 4 + 5 ; 6 + 7 ; 8 + 9

Output : 1, 5, 9, 13, 17

Answer :


2. Bambang adalah karyawan grosir X yang bertugas mengantarkan barang ke toko-toko pelanggannya yang terletak di satu ruas jalan panjang yang sama, dengan jarak masing-masing sebagai berikut (dihitung dari grosir X):
Toko 1: 0.5 km
Toko 2: 2 km
Toko 3: 3.5 km
Toko 4: 5 km

Jika Bambang menghabiskan waktu rata-rata 10 menit di setiap toko, dan laju rata-rata motor Bambang adalah 30 km/jam, berapa lama waktu yang dibutuhkan Bambang untuk mengantarkan pesanan hingga kembali ke grosir?

Input: 1-2-3-4
Output: 60 menit
clue: jarak total adalah 10 km

Input: 1-3-2-4-1
Output: 76 menit
clue: jarak total adalah 13 km


3. Berikut ini adalah record penjualan buah dalam bentuk string

Apel:1, Pisang:3, Jeruk:1, Apel:3, Apel:5, Jeruk:8, Mangga:1

Buat summary penjualannya

Input: string record penjualan
Output: Summary penjualan, dalam alphabetical order
    Apel: 9
    Jeruk: 9
    Mangga: 1
    Pisang: 3


4. Andi memiliki sejumlah uang dan wishlist di e-commerce. Jika Andi ingin membeli sebanyak mungkin barang dari wishlist-nya,
barang apa sajakah yang bisa dibeli Andi dengan uang tersebut? Jika ada barang dengan harga yang sama,
yang diprioritaskan adalah yang pertama di-input.

Input: Uang Andi, Jumlah barang, nama masing-masing barang dan harganya
Output: nama-nama barang yang bisa dibeli

Contoh:
Uang Andi: 100.000
Jumlah barang: 6
Nama barang:
    - Kaos batman, 50.000
    - Sepatu, 90.000
    - Dompet, 30.000
    - Pomade, 50.000
    - Casing handphone, 20.000
    - Cologne, 50.000
    
Output: Kaos batman, Casing handphone, Dompet


5. Dengan hanya menggunakan logic, ubah format jam dari 24H ke 12H dan juga sebaliknya

contoh:
input: 12:35 AM
output: 00:35

input: 19:30
output: 07:30 PM

input: 09:05
output: 09:05 AM

input: 11:30 PM
output: 23:30

NB: perbedaan format 12H dan 24H ada pada jam 00 tengah malam (jam 00 adalah jam 12 AM, lihat contoh 1), selebihnya tinggal menambahkan PM antara jam 12:00 - 23:59 dan AM antara jam 00:00 - 11:59


6. Devi memesan sebuah tas secara online, dan barang akan dikirimkan dalam waktu 7 hari kerja. 
Dari info hari dan tanggal pemesanan Devi, serta info hari libur nasional, maka pada hari dan tanggal berapakah pesanan Devi akan sampai?
Jika barang akan tiba di bulan berikutnya, tambahkan keterangan (tanggal x di bulang berikutnya)
Constraints: - Asumsikan ada 31 hari dalam 1 bulan
	     - Jika di bulan itu tidak ada hari libur Nasional, tulis 0.
	     - Di bulan berikutnya tidak ada hari libur Nasional.

Contoh:
Input: - Tanggal dan Hari pemesanan : 25 Sabtu
       - Hari libur Nasional: 26, 29
Output: Tanggal 5 di bulan berikutnya


7. Jim diminta untuk menyusuri lintasan linier dengan lubang-lubang yang harus dilompati. 
Jika Jim memilih untuk berjalan, dia akan berpindah tempat sebesar 1 dan mendapatkan energi sebesar 1, tetapi jika dia melompat dia akan berpindah tempat sebesar 2 dan kehilangan energi sebesar 2. 
Di awal, energi Jim adalah 0. Dengan kombinasi jalan-lompat yang kamu pilih, apakah Jim mampu melewati lintasan itu? Jika mampu, berapakah energi akhirnya? (jika tidak mampu, tulis saja \'Jim died\').
Lubangnya selalu berada di antara jalan, tidak ada lubang berturut-turut. 
Jim boleh melompat kapanpun, tidak harus pada lubang saja. 
Jika Jim salah memperhitungkan lompat/jalan, dia bisa masuk ke lubang dan mati.

Input:
1. Pola lintasan (- melambangkan jalan, o melambangkan lubang)
2. Pilihan cara jalan (w jalan, j lompat)

Output: energi akhir

Contoh:
1. ---o-o-
2. wwwjj
3. Jim died (Jim tidak punya tenaga untuk melompati lubang kedua)

1. -----o----o----o-
2. wwwwwjwwwjwwwj
3. 5

1. -----o----o----o-
2. wwwwwjjwjwwwj
3. 1

1. -----o----o----o-
2. wwjwwj
3. Jim died (setelah lompatan pertama, Jim tidak punya tenaga untuk melewati lubang pertama)

1. -----o----o----o-
2. wwwwjwwjwj
3. Jim died (lompatan pertamanya mengakibatkan Jim jatuh tepat di lubang pertama)


8. Andi memiliki sejumlah uang. Dengan uang itu, ia bermaksud membeli sebuah kacamata dan sepotong baju. Jika Andi bermaksud menggunakan uangnya semaksimal mungkin, tentukan berapa uang yang dapat Andi belanjakan. Jika harga kedua barang di luar jangkauan Andi, keluarkan pesan "Dana tidak mencukupi".

Input :
Terdiri dari 1 integer (jumlah uang yang dimiliki Andi) dan 2 baris array yang masing-masing berisi 3 data harga kacamata dan harga baju

Contoh :
Uang Andi: 70
Harga kacamata: 34, 26, 44
Harga baju: 21, 39, 33
Output = 67

Constrains :
- Uangnya hanya 70 dan akan dipakai semaksimal mungkin (sum <= 70)
- Ada 2 item yang akan dibeli (baju + kacamata) 


9. Buatlah fungsi untuk kalkulasi tarif parkir berdasarkan ketentuan berikut
Ketentuan tarif:
1. Delapan jam pertama: 1.000/jam
2. Lebih dari 8 jam s.d. 24 jam: 8.000 flat
3. Lebih dari 24 jam: 15.000/24 jam dan selebihnya mengikuti ketentuan pertama dan kedua

Input: 
- Tanggal dan jam masuk
- Tanggal dan jam keluar

Output: besarnya tarif parkir

Contoh: -Masuk: 28 Januari 2020 07:30:34
	-Keluar: 28 Januari 2020 20:03:35

Output: 8000

Penjelasan: Lamanya parkir adalah 12 jam 33 menit 1 detik, sehingga perhitungan tarif parkir dibulatkan menjadi 13 Jam.
	    Mengacu pada ketentuan kedua, maka yang harus dibayarkan adalah 8000 rupiah.


10. Jika 1 botol = 2 gelas, 1 teko = 25 cangkir, 1 gelas = 2.5 cangkir.
Buatlah sistem konversi volume berdasarkan data di atas
Contoh: 1 botol = .. cangkir?
	1 botol = 5 cangkir

11.
Setiap transaksi beli pulsa akan mendapatkan point dengan detail sebagai berikut :

0 - 10.000 -> 0 point
10.001 - 30.000 -> 1 point setiap kelipatan 1.000
> 30.000 -> 2 point setiap kelipatan 1.000
Tentukan point yang didapat oleh para pembeli

Contoh :

Beli pulsa Rp. 20.000
0 - 10.000 -> 0 point
10.001 - 20.000 -> 10.000 / 1.000  = 10 point
Output : 0 + 10 = 10 point

Beli pulsa Rp. 75.000
0 - 10.000 -> 0 point
10.001 - 30.000 -> 20.000 / 1.000  = 20 point
30.001 - 75.000 -> (45.000 / 1.000) * 2 = 90 point
Output : 0 + 20 + 90 = 110 point


12.
Huruf alfabet dalam huruf kecil di bawah ini mengandung bobot yang sudah ditentukan sebagai berikut:
a = 1; b = 2; c = 3; d = 4; .... Z = 26.
Tentukan apakah dalam sebuah input string sudah memiliki bobot yang sesuai.

Constraint :
-    0 <= n <= 100
-    string hanya mengandung huruf kecil

Input
string : mengandung kata/kalimat
array n : mengandung array angka yang harus dicocokkan terhadap string

Example
string : abcdzzz
array : [1, 2, 2, 4, 4, 26, 26]

Output : true, true, false, true, false, true, true

Explanation :
a = 1 -> true
b = 2 - > true
c = 3 -> false
d = 4 -> true
z = 4 -> false
z = 26 -> true
z = 26 -> true

13.
Anda akan menggunting-gunting tali sepanjang z meter menjadi beberapa buah tali sepanjang x meter. Berapa kali sedikitnya anda akan menggunting tali tersebut ?
Contoh: z = 4, x = 1
Cukup menggunting 2x (pertama, tali 4m dibagi 2 sama rata, akan didapatkan masing-masing 2m. Kemudian kedua tali 2m itu dipotong bersama sama rata, akan dihasilkan 4 tali masing-masing panjang 1m)

