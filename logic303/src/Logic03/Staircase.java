package Logic03;

import java.util.Scanner;

public class Staircase {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("n: ");
        int n = input.nextInt();
        // prerequisite
        String[][] array2D = new String[n][n];

        for (int i = 0; i< array2D.length; i++) {
            for (int j = 0; j< array2D[0].length; j++) {
                if ((i+j)>=n-1) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
