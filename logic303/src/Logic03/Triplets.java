package Logic03;

import java.util.Scanner;

public class Triplets {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter the value of the Triplets");
        int[][] array = new int[2][3];
        int a = 0;
        int b = 0;

        // enter value of array
        int i,j;
        for(i = 0; i < array.length; i++) {
            for(j = 0; j < array[0].length; j++) {
                System.out.print("Enter values for row " + i + " and column " + j + ": ");
                int deret = input.nextInt();
                array[i][j] = deret;
            }
        }

        // prerequisite
        for(i = 0; i < array[0].length; i++) {
            if (array[0][i] > array[1][i]) {
                a++;
            } else if (array[0][i] < array[1][i]) {
                b++;
            } else {
                //no additional point
                a = a;
                b = b;
            }
        }

        // print array
        for(i = 0; i < array.length; i++) {
            for(j = 0; j < array[0].length; j++) {
                System.out.print(array[i][j] + " ");
            }

            System.out.println();
        }

        System.out.println("Alice receives " + a + " points");
        System.out.println("Bob receives " + b + " points");
    }
}
