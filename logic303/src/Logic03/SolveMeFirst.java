package Logic03;

import java.util.Scanner;

public class SolveMeFirst {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        System.out.print("a: ");
        int a = input.nextInt();
        System.out.print("b: ");
        int b = input.nextInt();
        int sum = a + b;
        System.out.println("The sum of a + b = " + sum);
    }
}
