package Logic03;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while(answer.toUpperCase().equals("Y"))
        {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number) {
                case 1 -> SolveMeFirst.Resolve();
                case 3 -> SimpleArraySum.Resolve();
                case 4 -> DiagonalDifference.Resolve();
                case 5 -> PlusMinus.Resolve();
                case 6 -> Staircase.Resolve();
                case 7 -> MinMaxSum.Resolve();
                case 8 -> BirthdayCakeCandles.Resolve();
                case 9 -> BigSum.Resolve();
                case 10 -> Triplets.Resolve();
                case 11 -> Median.Resolve();
                case 12 -> Modus.Resolve();
                default -> System.out.println("No question available");
            }

            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }

        System.out.println("Thank you. See you soon");

    }
}