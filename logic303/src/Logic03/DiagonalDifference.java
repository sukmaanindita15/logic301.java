package Logic03;

import java.util.Scanner;

public class DiagonalDifference {
    public static void Resolve() {
        // input the element of matrix
        Scanner input = new Scanner(System.in);
        System.out.println("Input the element of matrix: ");
        int n = input.nextInt();
        int[][] matrixArray = new int[n][n];

        int d1 = 0;
        int d2 = 0;

        // prerequisite
        int i,j;
        for (i = 0; i < matrixArray.length; ++i) {
            for (j = 0; j < matrixArray[0].length; ++j) {

                System.out.println("The answer: ");
                int number = input.nextInt();
                matrixArray[i][j] = number;

                //computing diagonal 1 & diagonal 2
                if (i == j) {
                    d1 += matrixArray[i][j];
                }
                if (i+j == n - 1) {
                    d2 += matrixArray[i][j];
                }
            }

        }

        // print array
        for (i = 0; i < matrixArray.length; ++i) {
            for (j = 0; j < matrixArray[0].length; ++j) {
                System.out.print(matrixArray[i][j] + " ");
            }
            System.out.println();
        }

        int answer = d1 - d2;
        System.out.println();
        System.out.print(Math.abs(answer));
    }
}
