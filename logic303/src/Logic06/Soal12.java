package Logic06;

import java.util.Scanner;

public class Soal12 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);

        //input string untuk dicek
        System.out.print("Input string : ");
        String kata = input.nextLine();
        char[] ArrayKata = kata.toCharArray();

        //input array
        int[] arrayint = new int[ArrayKata.length];
        for (int i = 0; i < ArrayKata.length; i++) {
            System.out.print("Input nilai Array ke-"+(i+1)+" : ");
            arrayint[i] = input.nextInt();
        }

        //deklarasi huruf
        String hurufkecil ="abcdefghijklmnopqrstuvwxyz"; //array mulai dari 0 [0,1,2,3,4...
        String hurufbesar ="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        //cek sama atau tidak dengan array yang dimasukkan
        int helper=0;
        String benar ="True";
        String salah ="False";
        String[] hasilbobot =new String[ArrayKata.length];
        System.out.println("Output : ");
        for (int i = 0; i < ArrayKata.length ; i++) { //makanya ini dikurangi 1
            if((ArrayKata[i]==hurufbesar.charAt(arrayint[i]-1))||(ArrayKata[i]==hurufkecil.charAt(arrayint[i]-1))){
                hasilbobot[i]=benar;
                helper++;
            }else{
                hasilbobot[i]=salah;
                helper++;
            }
            System.out.print(hasilbobot[i]+", ");
        }
    }
}
