package Logic06;

import java.util.Arrays;
import java.util.Scanner;

public class Soal03 {
    public static void Resolve()
    {
        System.out.print("Banyak Transaksi : ");
        Scanner input = new Scanner(System.in);
        int banyakInput = input.nextInt();

        String[] recordPenjualan = new String[banyakInput];
        String[][] recordPenjualanSplit = new String[banyakInput][2];
        System.out.println();

        int helper = 0;
        int i = 0;

        while(true) {
            System.out.printf("Input transaksi ke-%d = ", helper + 1);
            Scanner input2 = new Scanner(System.in);
            String inputPenjualan = input2.nextLine();
            if (inputPenjualan.contains(":")) {
                recordPenjualan[helper] = inputPenjualan;
                helper++;
            } else {
                System.out.println("Salah format!");
            }

            if (helper == banyakInput) {
                System.out.println();

                String[] buahList;
                for(i = 0; i < banyakInput; i++) {
                    buahList = recordPenjualan[i].split(":");

                    for(int j = 0; j < buahList.length; j++) {
                        recordPenjualanSplit[i][j] = buahList[j];
                    }
                }

                String[] buah = new String[banyakInput];

                for(i = 0; i < banyakInput; i++) {
                    buah[i] = recordPenjualanSplit[i][0].toLowerCase();
                }

                buahList = (String[]) Arrays.stream(buah).distinct().toArray((x$0) -> {
                    return new String[x$0];
                });
                String[][] summaryRecord = new String[buahList.length][2];

                int j;
                for(i = 0; i < buahList.length; i++) {
                    int sumPenjualan = 0;

                    for(j = 0; j < recordPenjualanSplit.length; j++) {
                        if (recordPenjualanSplit[j][0].toLowerCase().equals(buahList[i])) {
                            sumPenjualan += Integer.parseInt(recordPenjualanSplit[j][1]);
                            summaryRecord[i][0] = buahList[i];
                            summaryRecord[i][1] = Integer.toString(sumPenjualan);
                        }
                    }
                }

                System.out.println("--- SUMMARY PENJUALAN ---");

                for(i = 0; i < summaryRecord.length; i++) {
                    for(j = 0; j < summaryRecord[0].length; j++) {
                        System.out.print(summaryRecord[i][j] + " ");
                    }

                    System.out.println();
                }

                return;
            }

            i++;
        }
    }
}
