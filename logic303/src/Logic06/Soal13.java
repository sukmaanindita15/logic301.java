package Logic06;

import java.util.Scanner;

public class Soal13 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Berapa meter panjang tali Anda yang akan digunting? ");
        int panjangTali = input.nextInt();
        System.out.print("menjadi berapa buah tali sepanjang berapa? ");
        int potongTali = input.nextInt();
        if (panjangTali % potongTali != 0) {
            System.out.println("Hasil Tidak bisa dibagi");
        } else if (panjangTali == potongTali) {
            System.out.println("0 ");
        } else {
            int hasil, hasil2, hasilAkhir=0;
            if (potongTali == 1) {
                if (panjangTali>potongTali){
                    hasil = panjangTali / 2;
                    hasil2 = panjangTali % 2;
                    hasilAkhir = hasil + hasil2;
                }
                System.out.println("Cukup menggunting " +hasilAkhir+ "x");
            } else if (potongTali > 1) {
                if (panjangTali>potongTali){
                    hasil = panjangTali / 2;
                    hasil2 = panjangTali % 2;
                    hasilAkhir = (hasil + hasil2)-1;
                }
                System.out.println("Cukup menggunting " +hasilAkhir+ "x");
            }
        }

    }
}
