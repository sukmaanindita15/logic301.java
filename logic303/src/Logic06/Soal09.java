package Logic06;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Soal09 {
        public class cova {
            public static void main(String[] args) throws ParseException {
                //Input Tanggal masuk&keluar
                Scanner input = new Scanner(System.in);
                System.out.println("Ex format input : 31-Dec-2020 23:37:50");
                System.out.println("Tanggal Masuk = ");
                String TanggalMasuk = input.nextLine();
                SimpleDateFormat KonversiMasuk = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
                Date KonversiTanggalMasuk = KonversiMasuk.parse(TanggalMasuk);
                System.out.println(KonversiTanggalMasuk);
                System.out.println("Tanggal Keluar = ");
                String TanggalKeluar = input.nextLine();
                SimpleDateFormat KonversiKeluar = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
                Date KonversiTanggalKeluar = KonversiKeluar.parse(TanggalKeluar);
                System.out.println(KonversiTanggalKeluar);

                //Hitung selisih
                long selisih = Math.abs(KonversiTanggalKeluar.getTime() - KonversiTanggalMasuk.getTime());
                long LamaParkir = TimeUnit.HOURS.convert(selisih, TimeUnit.MILLISECONDS);
                System.out.println("Lama Parkir = " + LamaParkir + "jam");
                int biaya;
                int hitung = (int) (LamaParkir / 24);
                if (LamaParkir <= 8) {
                    biaya = (int) (LamaParkir * 1000);
                    System.out.println(biaya);
                } else if (LamaParkir > 8 && LamaParkir < 24) {
                    biaya = 16000;
                    System.out.println(biaya);
                } else if (LamaParkir % 24 == 0) {
                    biaya = hitung * 15000;
                    System.out.println(biaya);
                } else if (LamaParkir > 24 && (LamaParkir - (hitung * 24) <= 8)) {
                    biaya = (int) ((hitung * 15000) + ((LamaParkir - (24 * hitung)) * 1000));
                    System.out.println(biaya);
                } else if (LamaParkir > 24 && (LamaParkir - (hitung * 24) > 8)) {
                    biaya = (int) ((hitung * 15000) + 16000);
                    System.out.println(biaya);
                }
            }
        }
}