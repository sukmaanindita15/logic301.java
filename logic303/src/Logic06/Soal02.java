package Logic06;

import java.util.Scanner;

public class Soal02 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Time Spend");
        System.out.println("1. Grosir X");
        System.out.println("2. Toko 1");
        System.out.println("3. Toko 2");
        System.out.println("4. Toko 3");
        System.out.println("5. Toko 4");
        System.out.println("Input Route: ");
        String urutan = input.nextLine();

        String[] rute = urutan.split("-");
        int[] array = new int[rute.length];

        for (int i = 0; i < rute.length; i++) {
            array[i] = Integer.parseInt(rute[i]);
        }
        int distance = 0;
        int breaktime=0;

        int i;


        for(i = 0; i < array.length - 1; ++i) {
            if (array[i] == 1 && array[i + 1] == 2) {
                distance += 500;
                breaktime += 10;
            } else if (array[i] == 1 && array[i + 1] == 3) {
                distance += 2000;
                breaktime += 10;
            } else if (array[i] == 1 && array[i + 1] == 4) {
                distance += 3500;
                breaktime += 10;
            } else if (array[i] == 1 && array[i + 1] == 5) {
                distance += 5000;
                breaktime += 10;
            } else if (array[i] == 2 && array[i + 1] == 1) {
                distance += 500;
                breaktime += 10;
            } else if (array[i] == 2 && array[i + 1] == 3) {
                distance += 1500;
                breaktime += 10;
            } else if (array[i] == 2 && array[i + 1] == 4) {
                distance += 3000;
                breaktime += 10;
            } else if (array[i] == 2 && array[i + 1] == 5) {
                distance += 3500;
                breaktime += 10;
            } else if (array[i] == 3 && array[i + 1] == 1) {
                distance += 2000;
                breaktime += 10;
            } else if (array[i] == 3 && array[i + 1] == 2) {
                distance += 1500;
                breaktime += 10;
            } else if (array[i] == 3 && array[i + 1] == 4) {
                distance += 1500;
                breaktime += 10;
            } else if (array[i] == 3 && array[i + 1] == 5) {
                distance += 3000;
                breaktime += 10;
            } else if (array[i] == 4 && array[i + 1] == 1) {
                distance += 3500;
                breaktime += 10;
            } else if (array[i] == 4 && array[i + 1] == 2) {
                distance += 3000;
                breaktime += 10;
            } else if (array[i] == 4 && array[i + 1] == 3) {
                distance += 1500;
                breaktime += 10;
            } else if (array[i] == 4 && array[i + 1] == 5) {
                distance += 1500;
                breaktime += 10;
            } else if (array[i] == 5 && array[i + 1] == 1) {
                distance += 5000;
                breaktime += 10;
            } else if (array[i] == 5 && array[i + 1] == 2) {
                distance += 4500;
                breaktime += 10;
            } else if (array[i] == 5 && array[i + 1] == 3) {
                distance += 3000;
                breaktime += 10;
            } else if (array[i] == 5 && array[i + 1] == 4) {
                distance += 1500;
                breaktime += 10;
            }
        }
        int waktuTempuh = distance/500;
        int waktuDrop = breaktime-10;

        System.out.println("Travel Time = " + waktuTempuh + " minutes");
        System.out.println("Break Time = " + waktuDrop + " minutes");
        System.out.println("Time Spend = " + (waktuTempuh+waktuDrop) + " minutes");
    }
}
