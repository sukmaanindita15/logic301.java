package Logic06;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve()
    {
        System.out.print("KONVERSI WAKTU");
        System.out.print("1\nPiliham Konversi : \n");
        System.out.print("1. 24H ke 12H \n");
        System.out.print("2. 12H ke 24H \n");
        char ulangMenu;
        do {
            Scanner input = new Scanner(System.in);
            System.out.print("Pilih konversi waktu : ");
            int inputPilih = input.nextInt();
            switch(inputPilih) {
                case 1:
                    System.out.print("Masukan Jam : ");
                    int jam24 = input.nextInt();
                    System.out.print("Masukan menit : ");
                    int menit24 = input.nextInt();
                    if(jam24 < 12 && menit24 <=59 ) {
                        System.out.print(("Pukul ") + jam24 + (":")+ menit24);
                        System.out.println("Konversi waktu " + jam24 +":" + menit24 + " AM");
                    }
                    else if(jam24 > 12 && jam24 <=23 && menit24 <=59) {
                        System.out.print(("Pukul ") + jam24 + (":")+ menit24);
                        System.out.println("Konversi waktu " + (jam24 - 12) +":" + menit24 + " PM");
                    }
                    else if((jam24 > 24) || (menit24 > 59)){
                        System.out.println("Format tidak valid");
                    }
                    break;
                case 2:
                    String StringAM = "AM";
                    String StringPM = "PM";
                    System.out.print("Masukan Jam : ");
                    int jam12 = input.nextInt();
                    System.out.print("Masukan menit : ");
                    int menit12 = input.nextInt();
                    System.out.print("AM/PM : ");
                    String AMPM = input.next();
                    if(jam12 < 12 && menit12 <=59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                        System.out.print(("Pukul ") +jam12 + (":")+ menit12 +(" ")+AMPM);
                        System.out.print("\nKonversi waktu " + jam12 +":" + menit12);
                    }
                    else if(jam12 == 12 && menit12 <=59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                        System.out.print(("Pukul ") +jam12 + (":")+ menit12 +(" ")+AMPM);
                        System.out.print("\nKonversi waktu " + "00" +":" + menit12);
                    }
                    else if(jam12 < 12 && menit12 <=59 && AMPM.toLowerCase().equals(StringPM.toLowerCase())) {
                        System.out.print(("Pukul ") +jam12 + (":")+ menit12 +(" ")+AMPM);
                        System.out.print("\nKonversi waktu " + (jam12 + 12) +":" + menit12);
                    }
                    else if ((jam12 > 12) || (menit12 > 59)) {
                        System.out.print("\nFormat tidak valid");
                    }
                    break;
                default:
                    System.out.print("Inputan salah. Pilih angka 1 atau 2!");
            }

            do {
                System.out.print("\nMasih ingin input (y/n)? ");
                ulangMenu = input.next().charAt(0);
                if (ulangMenu != 'n' && ulangMenu != 'y') {
                    System.out.println("Maaf inputan tidak sesuai");
                }
            } while(ulangMenu != 'n' && ulangMenu != 'y');
        } while(ulangMenu != 'n' && ulangMenu == 'y');


    }
}
