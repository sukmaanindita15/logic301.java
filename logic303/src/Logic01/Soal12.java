package Logic01;

import java.util.Scanner;

public class Soal12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Nomor 05
        // Prompt input
        System.out.println("Please enter the value of n: ");
        int n = input.nextInt();

        // Prerequisite
        int[] arrayInteger = new int[n];
        int helper = 2;
        int hitung = 1;
        boolean prima;

        // Enter the value to array variable
        while (hitung <= n) {
            prima = false;
            if (helper==2 || helper==3 || helper==5) {
                prima = true;
            } else if (helper%2!=0) {
                if (helper%3!=0) {
                    if (helper%5!=0) {
                        prima = true;
                    }
                }
            }
            if (prima==true) {
                System.out.print(helper+" ");
                hitung++;
            }
            helper++;
        }

        // Print array
	        /*for (int i = 0; i < n; i++) {
	        	if(i%2==0) {
	        		System.out.print(helper + " ");
	        	} */
        System.out.println();
    }
}
