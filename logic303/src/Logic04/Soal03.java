package Logic04;

import java.util.Scanner;

public class Soal03 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        char[] arrayinput = input.next().toCharArray();
        System.out.println("Rotasi sebanyak: ");
        int rotasi = input.nextInt();

        for(int i = 0; i < arrayinput.length; ++i) {
            int array3;
            char hasil3;
            if (arrayinput[i] >= 'a' & arrayinput[i] <= 'z' || arrayinput[i] >= 'A' & arrayinput[i] <= 'Z') {
                array3 = arrayinput[i] + rotasi;
                hasil3 = (char)array3;
                System.out.print(hasil3);
            }

            if (arrayinput[i] > 122 - rotasi) {
                array3 = 96 + (rotasi - (122 - arrayinput[i]));
                hasil3 = (char)array3;
                System.out.print(hasil3);
            }

            if (arrayinput[i] > 90 - rotasi) {
                array3 = 64 + (rotasi - (90 - arrayinput[i]));
                hasil3 = (char)array3;
                System.out.print(hasil3);
            }
        }
    }
}
