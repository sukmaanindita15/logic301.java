package Logic04;
import java.util.Scanner;

public class Soal11 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Input Word: ");
        String word = input.nextLine();
        String reverseStr = "";

                int wordLength = word.length();

                for (int i = (wordLength - 1); i >=0; i--) {
                    reverseStr = reverseStr + word.charAt(i);
                }

                if (word.toLowerCase().equals(reverseStr.toLowerCase())) {
                    System.out.println("YES");
                }
                else {
                    System.out.println("NO");
                }
    }
}
