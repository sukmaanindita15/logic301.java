package Logic04;

public class Soal12 {
    public static void Resolve() {
        String kalimat = "Aku Sayang Kamu";
        String[] kalimatSplit = kalimat.split(" ");
        char bintang = 42;
        String[] var3 = kalimatSplit;
        int var4 = kalimatSplit.length;

        for (int var5 = 0; var5 < var4; ++var5) {
            String a = var3[var5];
            System.out.println(a);
            char[] Char = a.toCharArray();

            for (int i = 0; i < Char.length; ++i) {
                if (i != 0 && i != Char.length - 1) {
                    Char[i] = (char) bintang;
                    System.out.print(Char[i] + " ");
                } else {
                    System.out.print(Char[i] + " ");
                }
            }

            System.out.println();
        }
    }
}
