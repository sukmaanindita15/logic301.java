package Logic04;

public class Soal02 {
    public static void Resolve() {
        String pass = "Java99GO";
        char[] Char = pass.toCharArray();
        String[] strongPass = new String[Char.length];
        int weak = 0;
        int space = 0;
        int upperCase = 0;
        int lowerCase = 0;
        int numbers = 0;
        int specialChar = 0;

        for(int i = 0; i < Char.length; ++i) {
            System.out.print(Char[i] + " ");
            if (Character.isWhitespace(Char[i])) {
                ++space;
            } else if (Character.isUpperCase(Char[i])) {
                ++upperCase;
            } else if (Character.isLowerCase(Char[i])) {
                ++lowerCase;
            } else if (Character.isDigit(Char[i])) {
                ++numbers;
            } else {
                ++specialChar;
            }
        }

        System.out.println();
        if (upperCase == 0) {
            ++weak;
        }

        if (lowerCase == 0) {
            ++weak;
        }

        if (numbers == 0) {
            ++weak;
        }

        if (specialChar == 0) {
            ++weak;
        }

        System.out.println();
        System.out.printf("Jumlah kelemahan password: %d", weak);
    }
}
