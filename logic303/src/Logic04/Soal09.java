package Logic04;

public class Soal09 {
    public static void Resolve() {
        String word1 = "cde";
        String word2 = "abc";
        char[] charArray1 = word1.toCharArray();
        char[] charArray2 = word2.toCharArray();
        char[] array1 = new char[charArray1.length];
        char[] array2 = new char[charArray2.length];
        int countDelete = 0;

        int i;
        for(i = 0; i < charArray1.length; ++i) {
            System.out.print(charArray1[i] + " ");
        }

        System.out.println();

        for(i = 0; i < charArray2.length; ++i) {
            System.out.print(charArray2[i] + " ");
        }

        System.out.println();
        System.out.println();

        for(i = 0; i < charArray1.length; ++i) {
            for(int j = 0; j < charArray2.length; ++j) {
                if (charArray1[i] == charArray2[j]) {
                    array1[i] = charArray1[i];
                    array2[j] = charArray2[j];
                }
            }
        }

        for(i = 0; i < array1.length; ++i) {
            System.out.print(array1[i] + " ");
            if (array1[i] == 0) {
                ++countDelete;
            }
        }

        System.out.println();

        for(i = 0; i < array2.length; ++i) {
            System.out.print(array2[i] + " ");
            if (array2[i] == 0) {
                ++countDelete;
            }
        }

        System.out.println();
        System.out.println(countDelete);
    }
}
