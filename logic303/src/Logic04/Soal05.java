package Logic04;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve() {
        System.out.println("Soal 05");
        System.out.println("HackerRank");
        System.out.println("Inputkan kata : ");
        Scanner input = new Scanner(System.in);
        String a = input.nextLine();
        if (a.contains("hackerrank")) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
