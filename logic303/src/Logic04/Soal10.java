package Logic04;

public class Soal10 {
    public static void Resolve() {
        String word1 = "hello";
        String word2 = "world";
        if (twoString(word1, word2)) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }

    }

    static boolean twoString(String word1, String word2) {
        char[] charArray1 = word1.toCharArray();
        char[] charArray2 = word2.toCharArray();
        boolean[] present = new boolean[charArray1.length];
        int countTrue = 0;

        int i;
        for(i = 0; i < charArray1.length; ++i) {
            System.out.print(charArray1[i] + " ");
        }

        System.out.println();

        for(i = 0; i < charArray2.length; ++i) {
            System.out.print(charArray2[i] + " ");
        }

        System.out.println();

        for(i = 0; i < charArray1.length; ++i) {
            for(int j = 0; j < charArray2.length; ++j) {
                if (charArray1[i] == charArray2[j]) {
                    present[i] = true;
                }
            }
        }

        for(i = 0; i < charArray1.length; ++i) {
            if (present[i]) {
                ++countTrue;
            }
        }

        if (countTrue == 0) {
            return false;
        } else {
            return true;
        }
    }
}
