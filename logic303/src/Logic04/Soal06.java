package Logic04;

import java.util.Scanner;

public class Soal06 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukan kalimat : ");
        String stringInput = input.nextLine();
        if (operationPangram(stringInput)) {
            System.out.println("Pangram");
        } else {
            System.out.println("Not Pangram");
        }

    }

    public static boolean operationPangram(String stringInput) {
        char[] alfabet = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        boolean[] daftarHadir = new boolean[alfabet.length];
        String stringConvert = stringInput.toLowerCase();
        char[] charArray = stringConvert.toCharArray();

        int i;
        for(i = 0; i < charArray.length; ++i) {
            for(int j = 0; j < alfabet.length; ++j) {
                if (charArray[i] == alfabet[j]) {
                    daftarHadir[j] = true;
                }
            }
        }

        for(i = 0; i < alfabet.length; ++i) {
            if (!daftarHadir[i]) {
                return false;
            }
        }

        return true;
    }
}
