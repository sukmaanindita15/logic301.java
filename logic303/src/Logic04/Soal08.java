package Logic04;

import java.util.Arrays;

public class Soal08 {
    public static void Resolve() {
        String word1 = "abddxr";
        String word2 = "baccdegxr";
        String word3 = "eeabcggx";
        char[] charArray1 = word1.toCharArray();
        char[] charArray2 = word2.toCharArray();
        char[] charArray3 = word3.toCharArray();

        int lengthMax;
        for (lengthMax = 0; lengthMax < charArray1.length; ++lengthMax) {
            System.out.print(charArray1[lengthMax] + " ");
        }

        System.out.println();

        for (lengthMax = 0; lengthMax < charArray2.length; ++lengthMax) {
            System.out.print(charArray2[lengthMax] + " ");
        }

        System.out.println();

        for (lengthMax = 0; lengthMax < charArray3.length; ++lengthMax) {
            System.out.print(charArray3[lengthMax] + " ");
        }

        System.out.println();
        if (charArray1.length > charArray2.length) {
            lengthMax = charArray1.length;
        } else if (charArray2.length > charArray3.length) {
            lengthMax = charArray2.length;
        } else {
            lengthMax = charArray3.length;
        }

        char[] eliminasi1 = new char[lengthMax];

        int i;
        for (i = 0; i < charArray1.length; ++i) {
            for (i = 0; i < charArray2.length; ++i) {
                if (charArray1[i] == charArray2[i]) {
                    eliminasi1[i] = charArray1[i];
                }
            }
        }

        String[] eliminasi2 = new String[lengthMax];

        for (i = 0; i < eliminasi1.length; ++i) {
            for (int j = 0; j < charArray3.length; ++j) {
                if (eliminasi1[i] == charArray3[j]) {
                    eliminasi2[i] = String.valueOf(charArray3[j]);
                }
            }
        }

        String[] uniqueChar = (String[]) Arrays.stream(eliminasi2).distinct().toArray((x$0) -> {
            return new String[x$0];
        });
        System.out.print(uniqueChar.length - 1);
    }
}
