package Logic04;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while(answer.toUpperCase().equals("Y")) {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number) {
                case 1 -> Soal01.Resolve();
                case 2 -> Soal02.Resolve();
                case 3 -> Soal03.Resolve();
                case 4 -> Soal04.Resolve();
                case 5 -> Soal05.Resolve();
                case 6 -> Soal06.Resolve();
                case 7 -> Soal07.Resolve();
                case 8 -> Soal08.Resolve();
                case 9 -> Soal09.Resolve();
                case 10 -> Soal10.Resolve();
                case 11 -> Soal11.Resolve();
                case 12 -> Soal12.Resolve();
                default -> System.out.println("No question available");
            }
            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }
        System.out.println("Thank you. See you soon");
    }
}
