package Logic05;

import java.util.Scanner;

public class GanjilGenap {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Input n: ");
        int n = input.nextInt();

        // identify the odd-ganjil
        for (int i=1; i<=n; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }

        //
        System.out.println();

        // identify the even-genap
        for (int i=1; i<=n; i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }
}
