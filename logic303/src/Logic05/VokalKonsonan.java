package Logic05;

import java.util.Arrays;
import java.util.Scanner;

public class VokalKonsonan {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);

        // inputting the word
        System.out.println("Enter the words: ");
        String word = input.nextLine();
        word = word.toLowerCase();
        String[] wordStringArray = word.split(" ");

        String vocal = "";
        String consonant = "";

        // looping for identify each alphabet, which vocal and consonant
        for (int i=0; i<wordStringArray.length; i++) {
            for (int j = 0; j<wordStringArray[i].length(); j++) {
                if (wordStringArray[i].charAt(j) == 'a' || wordStringArray[i].charAt(j) == 'i' ||
                        wordStringArray[i].charAt(j) == 'u' || wordStringArray[i].charAt(j) == 'e' ||
                        wordStringArray[i].charAt(j) == 'o') {
                    vocal = vocal+wordStringArray[i].charAt(j);
                } else {
                    consonant = consonant+wordStringArray[i].charAt(j);
                }
            }
        }

        char[] vocalChar = vocal.toCharArray();
        char[] consonantChar = consonant.toCharArray();

        // sorting, only for char
        Arrays.sort(vocalChar);
        Arrays.sort(consonantChar);

        System.out.print("Vocal Alphabet: ");
        System.out.println(vocalChar);
        System.out.print("Consonant Alphabet: ");
        System.out.println(consonantChar);
    }
}
