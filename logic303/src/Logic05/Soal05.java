package Logic05;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve()
    {
        System.out.println("List of Order");

        float sumMan = 0.0F;
        float sumWoman = 0.0F;
        float sumTeenager = 0.0F;
        float sumKid = 0.0F;
        float sumBaby = 0.0F;
        System.out.println("1. Man");
        System.out.println("2. Woman");
        System.out.println("3. Teenager");
        System.out.println("4. Kid");
        System.out.println("5. Baby");

        char callMenu;
        do {            //listing people
            System.out.println("Who's eating? (Choose 1-5)");
            Scanner input = new Scanner(System.in);
            int people = input.nextInt();
            switch (people) {
                case 1:
                    System.out.println("Man Quantity: ");
                    int Man = input.nextInt();
                    sumMan += (float) Man;
                    break;
                case 2:
                    System.out.println("Woman Quantity: ");
                    int Woman = input.nextInt();
                    sumWoman += (float) Woman;
                    break;
                case 3:
                    System.out.println("Teenager Quantity: ");
                    int Teenager = input.nextInt();
                    sumTeenager += (float) Teenager;
                    break;
                case 4:
                    System.out.println("Kid Quantity: ");
                    int Kid = input.nextInt();
                    sumKid += (float) Kid;
                    break;
                case 5:
                    System.out.println("Baby Quantity: ");
                    int Baby = input.nextInt();
                    sumBaby += (float) Baby;
                    break;
                default:
                    System.out.print("Please select the option 1-5!");
            }

            do {      //looping for listing the people
                System.out.println("Add more? (y/n)");
                callMenu = input.next().charAt(0);
                if (callMenu != 'n' && callMenu != 'y') {
                    System.out.println("Wrong Input");
                }
            } while (callMenu != 'n' && callMenu != 'y');
        } while(callMenu != 'n' && callMenu == 'y');

        // sumarize the sum of people
        float sumPeople = sumMan+sumWoman+sumTeenager+sumKid+sumBaby;

        // set the special condition for woman portion
        float womanPortion;
        if (sumPeople > 5.0F && sumPeople%2.0F == 1.0F) {
            womanPortion = sumWoman+1.0F;
        } else {
            womanPortion = sumWoman;
        }

        // set the condition for people
        float manPortion = sumMan * 2.0F;
        float teenagerPortion = sumTeenager * 1.0F;
        float kidPortion = sumKid * 1.0F/2.0F;
        float babyPortion = sumBaby * 1.0F;
        float sumPortion = manPortion+womanPortion+teenagerPortion+kidPortion+babyPortion;
        System.out.println("-Summary Order-");
        System.out.println("Man: " + (int)sumMan);
        System.out.println("Woman: " + (int)sumWoman);
        System.out.println("Teenager: " + (int)sumTeenager);
        System.out.println("Kid: " + (int)sumKid);
        System.out.println("Baby: " + (int)sumBaby);
        System.out.println("Total portion: " + (int)sumPortion);
    }
}
