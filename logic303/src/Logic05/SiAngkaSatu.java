package Logic05;

import java.util.Scanner;

public class SiAngkaSatu {

        static int hitungSumKuadrat(int getTampungangka) {
        int sumKuadrat = 0;
        String numberString = Integer.toString(getTampungangka);
        char[] charArray = numberString.toCharArray();
        int[] digitArray = new int[charArray.length];

        int i;
        for(i = 0; i < charArray.length; i++) {
            digitArray[i] = Character.getNumericValue(charArray[i]);
        }

        for(i = 0; i < digitArray.length; i++) {
            sumKuadrat = (int)(sumKuadrat + Math.pow(digitArray[i], 2));
        }
        return sumKuadrat;
    }

        public static void Resolve() {
        System.out.print("Masukan banyak deret (n) : ");
        Scanner input = new Scanner(System.in);
        int inputNumber = input.nextInt();
        int deret = 100;
        int helper = 0;

        while(true) {
            int tampungAngka = deret;

            do {
                tampungAngka = hitungSumKuadrat(tampungAngka);
            } while(tampungAngka >= 10);

            if (tampungAngka == 1) {
                helper++;
                System.out.println(deret);
            }

            if (helper == inputNumber) {
                break;
            }

            deret++;
        }
    }
}

