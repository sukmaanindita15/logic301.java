package Logic05;

import java.util.Scanner;

public class Soal04 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Fuel Usage");
        System.out.println("1. Toko");
        System.out.println("2. Tempat 1");
        System.out.println("3. Tempat 2");
        System.out.println("4. tempat 3");
        System.out.println("5. Tempat 4");
        System.out.println("Route Quantity: ");
        int route = input.nextInt();
        int[] array = new int[route];
        int distance = 0;

        int i;
        for(i = 0; i < array.length; ++i) {
            System.out.println("Route = (input 1-5)");
            int routeOption = input.nextInt();
            array[i] = routeOption;
        }

        for(i = 0; i < array.length - 1; ++i) {
            if (array[i] == 1 && array[i + 1] == 2) {
                distance += 2000;
            } else if (array[i] == 1 && array[i + 1] == 3) {
                distance += 2500;
            } else if (array[i] == 1 && array[i + 1] == 4) {
                distance += 4000;
            } else if (array[i] == 1 && array[i + 1] == 5) {
                distance += 6500;
            } else if (array[i] == 2 && array[i + 1] == 1) {
                distance += 500;
            } else if (array[i] == 2 && array[i + 1] == 3) {
                distance += 500;
            } else if (array[i] == 2 && array[i + 1] == 4) {
                distance += 2000;
            } else if (array[i] == 2 && array[i + 1] == 5) {
                distance += 4500;
            } else if (array[i] == 3 && array[i + 1] == 1) {
                distance += 2500;
            } else if (array[i] == 3 && array[i + 1] == 2) {
                distance += 1500;
            } else if (array[i] == 3 && array[i + 1] == 4) {
                distance += 1500;
            } else if (array[i] == 3 && array[i + 1] == 5) {
                distance += 4000;
            } else if (array[i] == 4 && array[i + 1] == 1) {
                distance += 4000;
            } else if (array[i] == 4 && array[i + 1] == 2) {
                distance += 2000;
            } else if (array[i] == 4 && array[i + 1] == 3) {
                distance += 1500;
            } else if (array[i] == 4 && array[i + 1] == 5) {
                distance += 2500;
            } else if (array[i] == 5 && array[i + 1] == 1) {
                distance += 6500;
            } else if (array[i] == 5 && array[i + 1] == 2) {
                distance += 4500;
            } else if (array[i] == 5 && array[i + 1] == 3) {
                distance += 4000;
            } else if (array[i] == 5 && array[i + 1] == 4) {
                distance += 2500;
            }
        }

        System.out.println("Fuel = " + distance / 2500 + " liter");
    }
}
