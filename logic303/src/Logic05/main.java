package Logic05;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while(answer.toUpperCase().equals("Y")) {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number) {
                case 1 -> GanjilGenap.Resolve();
                case 2 -> VokalKonsonan.Resolve();
                case 3 -> SiAngkaSatu.Resolve();
                case 4 -> Soal04.Resolve();
                case 5 -> Soal05.Resolve();
                case 6 -> BankTransfer.Resolve();
                case 7 -> GameCard.Resolve();
                case 8 -> DeretAngka.Resolve();
                case 9 -> Soal09.Resolve();
                case 10 -> SaldoOPO.Resolve();
                default -> System.out.println("No question available");
            }
            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }
        System.out.println("Thank you. See you soon");
    }
}
